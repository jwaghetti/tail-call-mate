package com.waghetti.tailcallmate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FactorialTest {

  private Factorial cut = new Factorial();

  @Test
  void testFactorialOne() {
    assertTrue(1 == cut.factorial(1));
  }

  @Test
  void testFactorialTwo() {
    assertTrue(2 == cut.factorial(2));
  }

  @Test
  void testFactorialThree() {
    assertTrue(6 == cut.factorial(3));
  }

  @Test
  void testFactorialFour() {
    assertTrue(24 == cut.factorial(4));
  }

  @Test
  void testFactorialFive() {
    assertTrue(120 == cut.factorial(5));
  }

}

class Factorial {

  public int factorial(int n) {
    return factorialTail(1, n).processRecursion();
  }

  private TailCall<Integer> factorialTail(int fact, int n) {
    if (n == 1) {
      return TailCallMate.prepareResult(fact);
    } else {
      return TailCallMate.prepareNextStep(() -> factorialTail(fact * n, n - 1));
    }
  }

}
