package com.waghetti.tailcallmate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CounterTest {

  private final Counter cut = new Counter();

  @Test
  void testFactorialOne() {
    assertEquals("0 1 ", cut.countFromZeroTo(1));
  }

  @Test
  void testFactorialFive() {
    assertEquals("0 1 2 3 4 5 ", cut.countFromZeroTo(5));
  }

}

class Counter {

  public String countFromZeroTo(final int value) {
    return countFromZeroToRec(0, value, new StringBuilder()).processRecursion();
  }

  private TailCall<String> countFromZeroToRec(int index, final int value, final StringBuilder stringBuilder) {

    stringBuilder.append(index + " ");

    if (index++ == value) {
      return TailCallMate.prepareResult(stringBuilder.toString());
    }

    int i = index;
    return TailCallMate.prepareNextStep(() -> countFromZeroToRec(i, value, stringBuilder));

  }

}
