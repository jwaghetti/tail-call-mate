package com.waghetti.tailcallmate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ErrorTest {

 private TailCallMate<String> cut = new TailCallMate<>();

 @Test
 void testNoResult() {
   assertThrows(
       TailCallError.class,
       ()-> cut.prepareNextStep( ()->fakeCall() ).getResult());
 }

  @Test
  void testCallAfterResult() {
    assertThrows(
        TailCallError.class,
        ()-> cut.prepareResult("A").processRecursionStep());
  }

 private static TailCall fakeCall() {
   throw new TailCallError("blah blah");
 }

}
