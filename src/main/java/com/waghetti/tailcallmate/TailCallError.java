package com.waghetti.tailcallmate;

public class TailCallError extends Error {

  protected TailCallError(String message) {
    super(message);
  }

}
