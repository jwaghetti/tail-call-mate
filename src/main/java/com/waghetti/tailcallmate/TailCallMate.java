package com.waghetti.tailcallmate;

public class TailCallMate<T> {

  public static <T> TailCall<T> prepareNextStep(TailCall<T> function) {
    return function;
  }

  public static <T> TailCall<T> prepareResult(T result) {
    return new DoneTailCall(result);
  }


  private static class DoneTailCall<T> implements TailCall<T> {

    private final T result;

    protected DoneTailCall(T result) {
      this.result = result;
    }

    @Override
    public TailCall<T> processRecursionStep() {
      throw new TailCallError("Can not call a result");
    }

    @Override
    public boolean isDone() {
      return true;
    }

    public T getResult() {
      return result;
    }

  }

}
