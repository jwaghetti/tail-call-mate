package com.waghetti.tailcallmate;

import java.util.stream.Stream;

@FunctionalInterface
public interface TailCall<T> {

  TailCall<T> processRecursionStep();

  default boolean isDone() {
    return false;
  }

  default T getResult() {
    throw new TailCallError("No result");
  }

  default T processRecursion() {
    return Stream.iterate(this, TailCall::processRecursionStep)
        .filter(TailCall::isDone)
        .findFirst()
        .get()
        .getResult();
  }

}
